import discord
import json
import sys


class Config:
    def __init__(self, logger):
        """Set defauld config values"""
        self.logger = logger

        self.retry_count = 3
        self.separator = '------------------------------\n'
        self.embed = True
        self.conf_file = "armanator.json"
        self.token_file = "armanator_token.key"
        # Test version
        # CONF_FILE = "armanator_test.json"
        # TOKEN_FILE = "armanator_token_test.key"
        self.play_file = "things_to_play_with.txt"
        self.bot_prefix = ("!")

        self.links = ""
        self.game_servers = []
        self.bot_channel_id = ""
        self.bot_channel = discord.channel
        self.server_info_channel_id = ""
        self.server_info_channel = discord.channel

        self.client_id = ""
        self.token = ""

        self.discord_server_id = ""
        self.discord_server_name = ""
        self.discord_server = discord.Server

        self.server_info_refresh_secs = 60

        self.role_admin_name = ""
        self.role_admin = discord.Role

        self.db_file_name = ""

        # db (mission stats) data refresh interval in minutes
        self.db_info_refresh = 5

        self.current_image_url = ''
        self.load_conf()
        self.load_games()
        self.load_tokens()

    # TODO: Refactor the function, it's too complicated'
    def load_conf(self):
        """Loads bot configuration from CONF_FILE."""
        try:
            with open(self.conf_file) as file:
                data = json.load(file)
        except IOError:
            self.logger.error(f'Opening {self.conf_file} failed!')
            sys.exit(1)
        except json.JSONDecodeError as e:
            self.logger.error(f'JSON Decoding {self.conf_file} failed!\n{e}')
            sys.exit(1)

        if "links" in data:
            for link in data["links"]:
                self.links += link + '\n'
        if "game_servers" in data:
            for server in data["game_servers"]:
                self.game_servers.append(
                    {
                        "host": server["host"],
                        "query_port": int(server["query_port"]),
                        "port": server["port"]
                    })

        if "server_name" in data:
            self.discord_server_name = data["server_name"]
        if "server_id" in data:
            self.discord_server_id = data["server_id"]
        if "bot_channel_id" in data:
            self.bot_channel_id = data["bot_channel_id"]
        if "server_info_channel_id" in data:
            self.server_info_channel_id = data["server_info_channel_id"]
        if "server_info_refresh_secs" in data:
            self.server_info_refresh_secs = data["server_info_refresh_secs"]
        if "role_admin" in data:
            self.role_admin_name = data["role_admin"]
        if "embed" in data:
            self.embed = data["embed"]
        if "db_filename" in data:
            self.db_file_name = data["db_filename"]
        if "db_info_refresh" in data:
            self.db_info_refresh = data["db_info_refresh"]

    def load_games(self):
        try:
            with open("./things_to_play_with.txt") as file:
                self.games = file.readlines()
        except IOError:
            self.logger.error("Could not open things_to_play_with.txt")
            sys.exit(1)

    def load_tokens(self):
        """Loads bot credentials from TOKEN_FILE."""
        try:
            file = open(self.token_file)
            data = json.load(file)
        except IOError:
            self.logger.error(f'Opening failed! {self.token_file}')
            sys.exit(1)
        except json.JSONDecodeError as e:
            self.logger.error(f'JSON Decoding {self.token_file} failed!\n{e}')
            sys.exit(1)

        if "client_id" not in data:
            self.logger.error("Missing bot client_id !")
            sys.exit(1)
        else:
            self.client_id = data["client_id"]
        if "secret" not in data:
            self.logger.error("Missing bot secret !")
            sys.exit(1)
        else:
            self.token = data["secret"]

        self.logger.info("Token loaded succesfully!")
