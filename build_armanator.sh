#!/usr/bin/env bash
# you need to manually copy/create in the destination dir token.key, server.conf and links.conf files
docker stop $(docker ps -a | grep armanator | cut -d' ' -f1)
docker rm $(docker ps -a | grep armanator | cut -d' ' -f1)
docker rmi armanator
docker build -t armanator .
