import asyncio
from datetime import datetime
import json
import logging
from logging.handlers import RotatingFileHandler
import random
import os
import sys

import discord
from discord.ext.commands import Bot
from discord import Game, HTTPException
from valve.source.a2s import NoResponseError, ServerQuerier

from config import Config
from database import Database


logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = RotatingFileHandler('armanator.log', maxBytes=5*1024*1024,
                              backupCount=4, encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)
handler_stream = logging.StreamHandler(sys.stdout)
handler_stream.setFormatter(
    logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s')
)
logger.addHandler(handler_stream)

config = Config(logger)

db = Database(config, logger)

armanator = Bot(command_prefix=config.bot_prefix)


async def close(exit_code=0):
    await armanator.close()
    # await asyncio.get_event_loop().stop()
    sys.exit(exit_code)


# ----------------------------------------------------------------------------------------------------------------------
@armanator.event
async def on_ready():
    """ Initialization info, running loops"""
    logger.info('Logged in as')
    logger.info(armanator.user.name)
    logger.info(armanator.user.id)
    logger.info(config.separator)
    armanator.loop.create_task(change_game())
    armanator.loop.create_task(change_image())
    armanator.loop.create_task(refresh_server_info())
    armanator.loop.create_task(get_info_db())

    # get the server
    for server in armanator.servers:
        if server.id == config.discord_server_id or server.name == config.discord_server_name:
            config.discord_server = server
            break

    # find the bot channel
    config.bot_channel = armanator.get_channel(config.bot_channel_id)
    config.server_info_channel = armanator.get_channel(config.server_info_channel_id)

    # find the admin role
    for role in config.discord_server.roles:
        if role.name == config.role_admin_name:
            config.role_admin = role

    # if this is a restarted instance -> inform user about the restart
    if len(sys.argv) == 2 and sys.argv[1] == 'restart':
        await armanator.send_message(config.bot_channel, "Restarted :ballot_box_with_check:")
        logger.info("Restarted.")


async def change_game():
    """Change game name the bot is playing."""
    while True:
        try:
            await armanator.change_presence(
                game=Game(name="with " + random.choice(config.games))
            )
            await asyncio.sleep(random.randint(300, 3600))
        except HTTPException as e:
            logger.error(f'(change_game) HTTPException: {e}')


async def delete_messages(channel, limit):
    """Remove old messages on the discord channel."""
    msgs = []
    try:
        async for x in armanator.logs_from(channel, limit=limit):
            msgs.append(x)
    except HTTPException as e:
        logger.error(f'(delete_messages) HTTPException logs_from {e}')
        return False

    if len(msgs) > 1:
        try:
            await armanator.delete_messages(msgs)
        except HTTPException as e:
            logger.error(
                f'(delete_messages) HTTPException delete_messages {e}'
            )
            return False
    elif len(msgs) == 1:
        try:
            await armanator.delete_message(msgs[0])
        except HTTPException as e:
            logger.error(f'(delete_messages) HTTPException delete_message {e}')
            return False
    return True


def get_server_info(address):
    """Returns Valve server info."""
    try:
        with ServerQuerier((address["host"], address["query_port"])) as server:
            result = server.info()
            # if result == NoResponseError
            return result
    except NoResponseError as e:
        logger.error(f'(get_server_info) NoResponseError: {e}')
        return None
    except Exception as e:
        logger.error(f'(get_server_info) Exception: {e}')
        return None


def get_player_info(address):
    """Returns a string containing list of players and scores."""
    try:
        with ServerQuerier((address["host"], address["query_port"])) as server:
            result = ''
            players = server.players()
            caption = "**Score - Player**"
            players = sorted(players["players"], key=lambda p: p["score"], reverse=True)
            for player in players:
                result += "{score} - {name}\n".format(**player)
            if len(result) < 1:
                result = 'None\n'
            return caption, result
    except NoResponseError as e:
        logger.error(f'(get_player_info) NoResponseError: {e}')
        return None
    except Exception as e:
        logger.error(f'(get_player_info) Exception: {e}')
        return None


async def get_info_db():
    """Retrieves data for storing in the db."""
    while True:
        for address in config.game_servers:
            try:
                info = await retry(get_server_info, address)
                # only push data if someone is on the server
                if (info["player_count"] > 0):
                    db.push_data(info["server_name"], info["game"], int(info["player_count"]))
            except Exception as e:
                logger.error(f'(get_info_db) {e}')
        await asyncio.sleep(config.db_info_refresh * 60)


# returns formated output, if embed is True, returns embed,
# otherwise returns data as string
async def get_info(address, player_info=False, server_info=False, embed=False,
                   timestamp_info=False):
    result = ''
    try:
        info = await retry(get_server_info, address)
        server_name = "**{server_name}**".format(**info)
        server_data = ('Players: {player_count}/{max_players}\nMap: {map}\n'
                       'Mission: {game}\n\n'.format(**info))
        if player_info:
            caption_players, players = await retry(get_player_info, address)
            player_server_data = ('Players: {player_count}' +
                                  '/{max_players}\n'.format(**info))
        if timestamp_info:
            update_time = datetime.utcnow().strftime('%H:%M:%S')
            timestamp = f'Last update: {update_time} UTC'
    except Exception as e:
        logger.error(f'(get_info) {e}')
        return
    else:
        if config.embed:
            embed = discord.Embed(colour=discord.Color.blue())
            if server_info:
                embed.add_field(name=server_name, value=server_data)
            if player_info and not server_info:
                embed.add_field(name=server_name, value=player_server_data)
            if player_info:
                embed.add_field(name=caption_players, value=players)
            if timestamp_info:
                embed.set_footer(text=timestamp)
            return embed
        else:
            result = ''
            if server_info:
                result += server_name + '\n' + server_data
            if player_info and not server_info:
                result += server_name + '\n' + player_server_data
            if player_info:
                result += caption_players + '\n' + players
            if timestamp_info:
                result += '\n' + timestamp
            return result


def get_server_url(address):
    host, port = address['host'], address['port']
    """Return complete server URL for launching steam game."""
    address_string = f'Server address: {host}:{port}'
    address_url = f'steam://run/107410//-connect={host}%20-port={port}'
    return address_string, address_url


async def change_image():
    base_url = ('https://gitlab.com/ArmaOnUnix/Armanator' +
                '/raw/master/img/<number>.jpg?inline=false')
    no = str(random.randint(1, 9))
    if len(no) == 1:
        no = '0' + no
    config.current_image_url = base_url.replace('<number>', no)
    await asyncio.sleep(random.randint(100, 3600))


# FIX problem if IndexError -> edit=True -> bot tries to edit non-existing msg
# TODO: Refactor the function, it's too complicated'
async def refresh_server_info():
    """Refreshes server info for server info channel every x seconds."""
    await retry_async(delete_messages, config.server_info_channel, 100)

    edit = False

    while True:
        for i in range(len(config.game_servers)):
            address = config.game_servers[i]

            # prepare the message
            result = await get_info(address, server_info=True,
                                    player_info=True, embed=config.embed,
                                    timestamp_info=True
                                    )
            address_string, address_url = get_server_url(address)
            if result is None:
                logger.error("(!refresh_server_info) get_info returned None")
                return

            # edit existing message
            if edit:
                msgs = []

                # get all the messages
                try:
                    async for x in armanator.logs_from(config.server_info_channel, limit=100):
                        msgs.append(x)

                    if config.embed:
                        result.set_image(url=config.current_image_url)
                        await armanator.edit_message(msgs[i*-1-1], embed=result)
                    else:
                        if i < len(config.game_servers) - 1:
                            result += '\n' + config.separator
                        await armanator.edit_message(msgs[i*-1-1], result)
                # TODO index out of range Exception handling -> delete all, edit = False
                except IndexError as e:
                    logger.error(f'(refresh_server_info) Refresh IndexError: {e}')
                    await delete_messages(config.server_info_channel, 100)
                    edit = False
                except Exception as e:
                    logger.error(f'(refresh_server_info) Refresh Exception: {e}')
                    continue
            # write first message
            else:
                try:
                    if config.embed:
                        result.set_image(url=config.current_image_url)
                        await armanator.send_message(config.server_info_channel, embed=result)
                    else:
                        if i < len(config.game_servers) - 1:
                            result += '\n' + config.separator
                        await armanator.send_message(config.server_info_channel, result)
                except Exception as e:
                    logger.error(f'(refresh_server_info) First msg Exception: {e}')
                    # if the first messages were not written correctly,
                    # remove them and start again
                    await delete_messages(config.server_info_channel, 100)
                    break

            if i == len(config.game_servers)-1:
                edit = True

        # refresh every 60 seconds
        await asyncio.sleep(config.server_info_refresh_secs)


async def restart_program():
    await armanator.say('Restarting...')
    logger.debug('Restarting...')
    await asyncio.sleep(3)
    os.execl("/usr/bin/python3", "python3", "armanator.py", "restart")


# callback, calls the function f again if it ended with error
async def retry_async(f, *args):
    for i in range(config.retry_count):
        try:
            result = await f(*args)
            if result:
                return result
        except TypeError as e:
            logger.error(f'(retry_async) TypeError: {e}')
        await asyncio.sleep(2)
        logger.warning("Retry async")


# callback, calls the function f again if it ended with error
async def retry(f, *args):
    for i in range(config.retry_count):
        try:
            result = f(*args)
            if result:
                return result
        except TypeError as e:
            logger.error(f'(retry) TypeError: {e}')
        await asyncio.sleep(2)
        logger.warning("Retry")


# ~~~~~ Commands ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@armanator.command(name='8ball',
                   description='Wisdom from a certified* oracle.',
                   brief='Random answer.',
                   pass_context=True)
async def eight_ball(ctx):
    """8ball, because why not..."""
    answers = [
        'Nope',
        'Probably not',
        'I am not sure',
        'Maybe',
        'I guess so',
        'Definitely',
    ]
    await armanator.say(random.choice(answers))


@armanator.command(name='server',
                   description=('Shows information about the server' +
                                '(mission, maps, no of players).'),
                   brief='Shows server info.',
                   aliases=['servers'])
async def server():
    """Shows information about Valve server (mission, maps, no of players)."""
    for i in range(len(config.game_servers)):
        address = config.game_servers[i]
        result = await get_info(address, server_info=True, embed=config.embed)
        if result is None:
            logger.error("(!server) get_info returned None")
            return

        try:
            if config.embed:
                await armanator.say(embed=result)
            else:
                if i < len(config.game_servers) - 1:
                    result += '\n' + config.separator
                await armanator.say(result)
        except Exception as e:
            logger.error(f'(!server) Exception: {e}')


@armanator.command(name='players',
                   description='Shows players on the server.',
                   brief='Shows player info.',
                   aliases=['player'])
async def players():
    """Shows information about players."""
    for i in range(len(config.game_servers)):
        address = config.game_servers[i]
        result = await get_info(address, player_info=True, embed=config.embed)
        if result is None:
            logger.error("(!players) get_info returned None")
            return
        try:
            if config.embed:
                await armanator.say(embed=result)
            else:
                if i < len(config.game_servers) - 1:
                    result += '\n' + config.separator
                await armanator.say(result)
        except Exception as e:
            logger.error(f'(!players) Exception: {e}')


@armanator.command(name='all',
                   description=('Shows information about both' +
                                'the server and its players.'),
                   brief='Shows player and server info.',
                   aliases=['both'])
async def all():
    """Shows information about both the server and its players."""
    for i in range(len(config.game_servers)):
        address = config.game_servers[i]
        result = await get_info(address, server_info=True, player_info=True,
                                embed=config.embed)
        if result is None:
            logger.error("(!all) get_info returned None")
            return
        try:
            if config.embed:
                await armanator.say(embed=result)
            else:
                if i < len(config.game_servers) - 1:
                    result += '\n' + config.separator
                await armanator.say(result)
        except Exception as e:
            logger.error(f'(!all) Exception: {e}')


@armanator.command(name='info',
                   description='Shows important community URLs.',
                   brief='Shows important community URLs.',
                   aliases=['group', 'community'])
async def info():
    """Shows information about the AoU group."""
    try:
        if config.embed:
            embed = discord.Embed(colour=discord.Color.blue())
            embed.add_field(name='**Community links**', value=config.links)
            await armanator.say(embed=embed)
        else:
            await armanator.say(config.links)
    except Exception as e:
        logger.error(f'(!info) Exception: {e}')


@armanator.command(name='play',
                   description='Shows server connection info.',
                   brief='Shows server connection info.',
                   aliases=['game', 'connect'])
async def play():
    """Shows information about the AoU servers."""
    for i in range(len(config.game_servers)):
        address = config.game_servers[i]
        info = await retry(get_server_info, address)
        if info is None:
            logger.error("(!play) get_server_info returned None")
            return

        server_name = "**{server_name}**".format(**info)
        server_string, server_url = get_server_url(address)
        server_info = server_string + "\n" + server_url

        try:
            if config.embed:
                embed = discord.Embed(colour=discord.Color.blue())
                embed.add_field(name=server_name, value=server_info)
                await armanator.say(embed=embed)

            else:
                if i < len(config.game_servers)-1:
                    server_info += '\n' + config.separator
                await armanator.say(server_name + '\n' + server_info)
        except Exception as e:
            logger.error(f'(!play) Exception: {e}')


# ~~~~~ bot managing commands ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@armanator.command(name='shutdown',
                   description=('Turns off the Armanator bot.' +
                                ' (Admin only command)'),
                   brief='Turns off the Armanator bot. (Admin only)',
                   pass_context=True)
async def shutdown(ctx):
    """Turns off the Armanator bot. (Admin only command)"""
    author = ctx.message.author
    if config.role_admin in author.roles:
        await armanator.say(
            f'Are you sure you want to turn off the Armanator bot ? [y/n] - {author.mention}'
        )
        logger.debug(
            f'Are you sure you want to turn off the Armanator bot ? [y/n] - {author.mention}'
        )
        msg = await armanator.wait_for_message(author=author, channel=ctx.message.channel)
        if msg:
            if msg.content.lower() == 'y' or msg.content.lower() == 'yes':
                await armanator.say('Shutting down...')
                logger.debug('Shutting down...')
                await asyncio.sleep(3)
                await close(0)
            elif msg.content.lower() == 'n' or msg.content.lower() == 'no':
                await armanator.say('Shutdown cancelled.')
                logger.debug('Shutdown cancelled.')
            else:
                await armanator.say("User didn't give a valid option.")
                logger.debug("User didn't give a valid option.")
    else:
        await armanator.say('You are not allowed to use shutdown command! (Admin only command)')
        logger.debug(
            f'You are not allowed to use shutdown command! (Admin only command) - {author}'
        )


@armanator.command(name='restart',
                   description='Restarts the Armanator bot. (Admin only command)',
                   brief='Restarts the Armanator bot. (Admin only)',
                   pass_context=True)
async def restart(ctx):
    """Restarts the Armanator bot. (Admin only command)"""
    author = ctx.message.author
    if config.role_admin in author.roles:
        await armanator.say(
            f'Are you sure you want to restart the Armanator bot ? [y/n] - {author.mention}'
        )
        logger.debug(
            f'Are you sure you want to restart the Armanator bot ? [y/n] - {author.mention}'
        )
        msg = await armanator.wait_for_message(author=author, channel=ctx.message.channel)
        if msg:
            if msg.content.lower() == 'y' or msg.content.lower() == 'yes':
                await restart_program()
            elif msg.content.lower() == 'n' or msg.content.lower() == 'no':
                await armanator.say('Restart cancelled.')
                logger.debug('Restart cancelled.')
            else:
                await armanator.say("User didn't give a valid option.")
                logger.debug("User didn't give a valid option.")
    else:
        await armanator.say('You are not allowed to use restart command! (Admin only command)')
        logger.debug(f'You are not allowed to use restart command! (Admin only command) - {author}')


@armanator.command(name='update',
                   description=('Updates bot from the repository and restarts the bot.' +
                                '(Admin only command)'),
                   brief='Updates bot from the repository. (Admin only)',
                   pass_context=True)
async def update(ctx):
    """Updates the Armanator bot the newest git version. (Admin only command)"""
    author = ctx.message.author
    if config.role_admin in author.roles:
        await armanator.say(
            ('Are you sure you want to update and restart the Armanator bot ?' +
             f'[y/n] - {author.mention}')
        )
        logger.debug(
            ('Are you sure you want to update and restart the Armanator bot ?' +
             f'[y/n] - {author.mention}')
        )
        msg = await armanator.wait_for_message(author=author, channel=ctx.message.channel)
        if msg:
            if msg.content.lower() == 'y' or msg.content.lower() == 'yes':
                await armanator.say('Updating...')
                logger.info('Updating...')
                logger.info(os.system("git pull"))
                await restart_program()
            elif msg.content.lower() == 'n' or msg.content.lower() == 'no':
                await armanator.say('Update cancelled.')
                logger.info('Update cancelled.')
            else:
                await armanator.say("User didn't give a valid option.")
                logger.info("User didn't give a valid option.")
    else:
        await armanator.say('You are not allowed to use update command! (Admin only command)')
        logger.debug(f'You are not allowed to use update command! (Admin only command) - {author}')


@armanator.command(name='stats',
                   description='Shows basic Discord server stats.',
                   brief='Shows basic Discord server stats.',
                   pass_context=True)
async def stats(ctx, arg=''):
    """Shows information about its work - how many users have been promoted
    and how many have been punished."""
    # global stats
    # today -> todays stats, without arg -> this month,
    # name of month -> stats for that month
    for address in config.game_servers:
        try:
            info = await retry(get_server_info, address)
            if arg and type(arg) is int:
                data = db.get_stats(info["server_name"], int(arg))
                logger.info("Stats, arg: " + arg)
            else:
                data = db.get_stats(info["server_name"], 5)
                logger.info("Stats")

            if config.embed:
                embed = discord.Embed(title="**" + info["server_name"] + "**",
                                      colour=discord.Color.blue())
                missions = ''
                stats = ''
                # if there is some data in the database
                if (len(data) > 0):
                    for row in data:
                        missions += row[0] + "\n"
                        stats += f'\t{(row[1]/60):.1f} / {(row[2]):.1f}\n'
                    embed.add_field(name="**Mission**", value=missions, inline=True)
                    embed.add_field(name="**Playtime / Avg players**", value=stats, inline=True)
                # if the database is empty
                else:
                    embed.add_field(name="Nothing to show so far...", value=":poop:", inline=True)
                await armanator.say(embed=embed)
        except Exception as e:
            logger.error(f'(!stats) {e}')


# ~~~~~ bot managing commands ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# TODO
async def shutdown_game_server():
    pass


# TODO
async def restart_game_server():
    pass


armanator.run(config.token)
