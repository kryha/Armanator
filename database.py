# This file contains database which stores mission-related server data
import sqlite3
import sys
from datetime import datetime


class Database:
    def __init__(self, config, logger):
        self.load_db(config.db_filename)
        self.logger = logger
        self.config = config

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close_db()

    def load_db(self, file_name: str):
        try:
            self._conn = sqlite3.connect(file_name)
            self.create_tables()
        except Exception as e:
            self.logger.error(f'Database error: {e}.')
            sys.exit(1)

    def close_db(self):
        self.logger.info("Closing the database...")
        self._conn.close()

    def execute_query(self, query: str):
        try:
            self._conn.execute(query)
            self._conn.commit()
        except Exception as e:
            self.logger.error(f'Error executing query:\n{query}\n{e}')

    def execute_select_query_one(self, query: str):
        try:
            cur = self._conn.cursor()
            cur.execute(query)
            return cur.fetchone()
        except Exception as e:
            self.logger.error(f'Error executing query:\n{query}\n{e}')

    def execute_select_query(self, query: str):
        try:
            cur = self._conn.cursor()
            cur.execute(query)
            return cur
        except Exception as e:
            self.logger.error(f'Error executins query:\n{query}\n{e}')

    def create_tables(self):
        query = '''CREATE TABLE IF NOT EXISTS server(
                    id          INTEGER		PRIMARY KEY,
                    name        TEXT        NOT NULL);'''
        self._conn.execute(query)
        query = '''CREATE TABLE IF NOT EXISTS mission(
                    id			INTEGER		PRIMARY KEY,
                    name        TEXT        NOT NULL);'''
        self._conn.execute(query)
        query = '''CREATE TABLE IF NOT EXISTS mission_date(
                    id          INTEGER     PRIMARY KEY,
                    server_id   INTEGER,
                    mission_id  INTEGER,
                    timestamp   TEXT        NOT NULL,
                    playtime    REAL,
                    player_minute   REAL,
                    FOREIGN KEY(server_id)  REFERENCES server(id),
                    FOREIGN KEY(mission_id) REFERENCES mission(id));'''
        self._conn.execute(query)

    def commit(self):
        try:
            self._conn.commit()
        except Exception as e:
            self.logger.error("Error commitng: {}".format(e))

    def trim(self, s: str):
        s = s.replace("\'", "")
        s = s.replace('\"', '')
        return s

    def push_data(self, server_name: str, mission_name: str, no_players: int):
        server_name = self.trim(server_name)
        mission_name = self.trim(mission_name)

        try:
            # server
            server_id = self.execute_select_query_one(
                f"SELECT id FROM server WHERE name=\'{server_name}\'"
            )
            if server_id is None:
                self.execute_query(f'INSERT INTO server(name) VALUES(\'{server_name}\');')
                server_id = self.execute_select_query_one(
                    f"SELECT id FROM server WHERE name=\'{server_name}\'"
                )

            # mission
            mission_id = self.execute_select_query_one(
                f"SELECT id FROM mission WHERE name=\'{mission_name}\'"
            )
            if mission_id is None:
                self.execute_query(f'INSERT INTO mission(name) VALUES(\'{mission_name}\');')
                mission_id = self.execute_select_query_one(
                    f"SELECT id FROM mission WHERE name=\'{mission_name}\'"
                )

            # mission_date
            mission_id_, server_id_ = mission_id[0], server_id[0]
            timestamp = datetime.utcnow().date().strftime("%y-%m-%d")
            result_id = self.execute_select_query_one(f'''SELECT id FROM mission_date
                                                          WHERE mission_id={mission_id_}
                                                          AND server_id={server_id_}
                                                          AND timestamp=\'{timestamp}\'''')
            if result_id is None:
                self.execute_query(
                    f'''INSERT INTO mission_date(server_id, mission_id, timestamp, playtime,
                                                 player_minute)
                        VALUES({server_id_}, {mission_id_}, \'{timestamp}\', 0, 0)''')
            else:
                self.execute_query(
                    f'''UPDATE mission_date SET playtime = playtime+{self.config.db_info_refresh},
                        player_minute = player_minute+{self.config.db_info_refresh * no_players}
                            WHERE id={result_id[0]}''')
        except Exception as e:
            self.logger.error(f'(push_data) Error executing query: {e}')

    def get_stats(self, server_name: str, limit=5):
        try:
            server_name = self.trim(server_name)
            timestamp = datetime.utcnow().date().strftime("%y-%m")
            query = f'''SELECT m.name, SUM(md.playtime), SUM(md.player_minute)/SUM(md.playtime)
                        FROM mission_date md
                        INNER JOIN mission m ON m.id = md.mission_id
                        INNER JOIN server s ON s.id = md.server_id
                        WHERE s.name=\'{server_name}\'
                        AND timestamp LIKE \'{timestamp}%\'
                        GROUP BY m.name
                        ORDER BY md.playtime DESC
                        LIMIT {limit};'''
            cur = self.execute_select_query(query)
            return cur.fetchall()
        except Exception as e:
            self.logger.error(f'(get_stats) Error: {e}')
