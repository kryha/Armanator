**This README is WIP, it still contains parts from the old bot version!**

# Armanator

Little discord bot for informing users about Arma 3 server. This bot shows server name, 
player count and mission and map info.

#### How to use

**1.** Go into the directory where you want the bot files to reside ```cd ~/armanator```.

**2.** ```git clone https://gitlab.com/ArmaOnUnix/Armanator.git```

**3.** Now you need to manually create/copy here **armanator_token.key** and **armanator.json**. You can use **armanator_token_example.key** and **armanator_example.json** as reference.

**armanator_token.key** file contains 2 strings. The first string is your bot token and the second one is the 
channel ID where the server info is shown and automatically refreshed via the *refresh_server_info()*
function. Below you can see an example of the token.key file content:
```
SxuUC9M6uERK6D4J76a5iph6bqg3zFqSPYGbtz3mKdPHQQBGm4LW6PtpQh2
465239597899992949
```

**armanator.json** contains a list of your servers. Armanator will show information about them when 
someone uses its commands. Below you can see an example of the servers.conf file content:
```json
{
    // links shown after using the !info command
    "links": [
      "**Web page**: https://armaonunix.gitlab.io/",
      "**Discord**: https://discord.gg/p28Ra36",
      "**Steam group**: https://steamcommunity.com/groups/ArmaOnUnix",
      "**Reddit**: https://www.reddit.com/r/armaonunix/",
      "**Email**: armaonunix@gmail.com",
      "**Gitlab**: https://gitlab.com/groups/ArmaOnUnix"
    ],
    "game_servers":[
      {
        "host": "armaonunix.duckdns.org",
        "query_port": 2303,
        "port": 2302
      }
    ],

    "server_name": "ArmaOnUnix",
    "server_id": "239344443503804416",

    "bot_channel_id": "473186979031285771",
    "server_info_channel_id": "473201799323451405",

    "server_info_refresh_secs": 60,

    "role_admin": "Admin",

    "embed": true
}
```

**links.conf** contains a list of community links/urls shown by the info() function. 
Below you can see an example of the links.conf file content (uses markdown as its supported by Discord):
```
**Web page**: https://armaonunix.gitlab.io/
**Discord**: https://discord.gg/p28Ra36
**Steam group**: https://steamcommunity.com/groups/ArmaOnUnix
**Reddit**: https://www.reddit.com/r/armaonunix/
**Email**: armaonunix@gmail.com
**Gitlab**: https://gitlab.com/groups/ArmaOnUnix
```

**4.** Create a docker image with the bot by running:

```bash build_armanator.sh```.

**5.** When the docker image is ready, you can create a docker container and run the bot:

```bash run_armanator.sh```

**6.** Enjoy !



