FROM debian:stable

RUN apt-get update; apt-get -y upgrade
RUN apt-get install -yqq python3 python3-pip python3-virtualenv git
RUN pip3 install discord.py python-valve
#RUN cd /; git clone https://gitlab.com/ArmaOnUnix/Armanator.git
RUN mkdir /armanator
VOLUME /armanator
WORKDIR /armanator

STOPSIGNAL SIGINT
CMD python3 armanator.py
